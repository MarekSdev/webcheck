import sys
import requests
import time
import json

url, matcher = sys.argv[1], sys.argv[2]  # command line arguments

with open('config.json') as f:  # read config
    config = json.load(f)

raisedExceptions = -1

while True:
    try:
        response = requests.get(url, timeout=config["time"])
        if not matcher in str(response.content):
            raise Exception
        if raisedExceptions != 0:
            print("SITE UP")
            raisedExceptions = 0
    except:
        raisedExceptions += 1
    if(raisedExceptions == config["threshold"]):
        print("SITE DOWN")
    time.sleep(config["interval"])
