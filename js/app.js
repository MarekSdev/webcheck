const argv = require("yargs").argv;
const got = require("got");

let config = require("./config.json");

var url = argv.u,
  key = argv.q,
  siteUp = null;

function Fetch() {
  (async () => {
    try {
      const response = await got(url, {
        prefixUrl: "https://", // we prefer secure sites:)
        retry: {
          limit: config["threshold"],
        },
      }).on("request", (request) =>
        setTimeout(() => request.destroy(), config["time"])
      );
      if (!response.body.includes(key)) throw new Error("Match not found");
      if (!siteUp || siteUp == null) console.log("SITE UP");
      siteUp = true;
    } catch (error) {
      if (siteUp || siteUp == null) console.log("SITE DOWN");
      siteUp = false;
    }
  })();
}
setInterval(Fetch, config["interval"]);
