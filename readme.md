# WebCheck


### Ping website with keyword

Simple script(s) to ping website for a specific keyrword.

### Requirements


- url & matcher params via cli
- check for (status, response time, content match)
- read config from file (allowed response time, ping interval, notification threshold)
- continious loop
- notify user when the site is up / down (based on threshold)

### How to use 

**JS version**

`node app.js -u website -q matcher ` - **-u** website url (for example: www.google.com), **-q** query parameter (keyword to find on website)

**Python version**

`python3 webcheck.py url matcher`, full url (https://www.example.com)

### Config.json

JS - values are in ms

Python - values are in seconds
